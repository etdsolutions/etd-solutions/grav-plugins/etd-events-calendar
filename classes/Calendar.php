<?php

namespace Grav\Plugin\ETDEventsCalendar;

class Calendar
{
    /**
     * @return string Twig for displaying an empty calendar
     * @param array $translations A list of translated text, from the plugin languages file
     *
     * Returns Twig code for displaying an empty calendar on a page. In order to be displayed correctly, it needs to be evaluated in the template as raw Twig
     */
    public function displayCalendar(array $translations): string
    {
        $str = "";

        $firstDay = date("y-m-01");
        $firstWeekDay = (date("w", strtotime($firstDay)) + 6) % 7; //monday => 0 / sunday =>6

        $lastDay = date("y-m-t");
        $lastWeekDay = (date("w", strtotime($lastDay)) + 6) % 7; //monday => 0 / sunday =>6

        $day = 0;
        $started = false;

        $str .= "<p style=\"font-weight:bold;text-align:center;padding:10px 0\">Il vous est conseillé de disposer d’une tenue adaptée à la météo ainsi que des chaussures de marche. </p>
                    <div id='etd-calendar'>";

        $str .=
            "<div id='etd-calendar-legend'>
                    <div class='legend-elmt'><span id='etd-legend-open' class='legend-color open'></span><span class='legend-text'>" .
            $translations["LEGEND_AVAILABLE"] .
            "</span></div>
                    <div class='legend-elmt'><span id='etd-legend-event' class='legend-color working-group'></span><span class='legend-text'>" .
            $translations["LEGEND_WORKING_GROUP"] .
            "</span></div>
                    <div class='legend-elmt'><span id='etd-legend-formation' class='legend-color formation'></span><span class='legend-text'>" .
            $translations["LEGEND_FORMATION"] .
            "</span></div>
                    <div class='legend-elmt'><span id='etd-legend-privatisated' class='legend-color privatised'></span><span class='legend-text'>" .
            $translations["LEGEND_PRIVATISED"] .
            "</span></div>
                    <div class='legend-elmt'><span id='etd-legend-exterieur' class='legend-color exterieur'></span><span class='legend-text'>Guy BOULNOIS en extérieur</span></div>
                </div>";

        $str .=
            "<div id='etd-calendar-header'>
                    <span id='etd-calendar-header-month'></span>
                    <div id='etd-calendar-month-tools'>
                        <button id='etd-previous-month'><span class='fas fa-angle-left'></span></button>
                        <button id='etd-current-month'>" .
            $translations["CURRENT_MONTH"] .
            "</button>
                        <button id='etd-next-month'><span class='fas fa-angle-right'></span></button>
                    </div>
                </div>";

        $str .= "<div id='etd-calendar-body'>";

        //first week
        $str .= "<div id='week-n-0' class='week'>";
        for ($weekDay = 0; $weekDay < 7; $weekDay++) {
            $str .=
                "<a class='day day-n-" . $weekDay . "' id='date-" . $day . "'>";
            $str .= "<span class='date'></span>";
            $str .= "</a>";

            $day++;
        }
        $str .= "</div>";

        //remaining weeks
        for ($week = 1; $week < 6; $week++) {
            $str .= "<div id='week-n-" . $week . "' class='week'>";

            for ($weekDay = 0; $weekDay < 7; $weekDay++) {
                $str .=
                    "<a class='day day-n-" .
                    $weekDay .
                    "' id='date-" .
                    $day .
                    "'>";
                $str .= "<span class='date'></span>";
                $str .= "</a>";

                $day++;
            }

            $str .= "</div>";
        }
        $str .= "</div>";
        $str .= "</div>";

        return $str;
    }
}

?>
