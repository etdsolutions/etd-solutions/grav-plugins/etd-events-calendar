<?php

namespace Grav\Plugin\ETDEventsCalendar;

use DateTime;

class Events
{
    /**
     * @return string JSON list of the month's events
     * @param string $month The requested month (format: YY-MM)
     * @param array $events The complete events list, as found in the plugin configuration
     * @param string $formLink The path leading to the website's contact form, as found in the plugin configuration
     *
     * Returns the list of all the events taking place during the requested month
     */
    public function getMonthlyEvents(
        string $month,
        array $events,
        string $formLink
    ): string {
        $monthDate = strtotime($month . "-01");

        $reqYear = intval(explode("-", $month)[0]);
        $reqMonth = explode("-", $month)[1];

        $monthInfo = [];
        $monthInfo["month"] = $month;
        $monthInfo["monthDate"] = date("Y-m-d", $monthDate);
        $monthInfo["numberOfDays"] = date("t", $monthDate);
        $firstDay = date($month . "-01");
        $monthInfo["firstDay"] = (date("w", strtotime($firstDay)) + 6) % 7;
        $lastDay = date($month . "-t");
        $monthInfo["lastDay"] = (date("w", strtotime($lastDay)) + 6) % 7;
        $monthInfo["formLink"] = $formLink;

        $res = [];
        $res["monthInfo"] = $monthInfo;
        $res["events"] = array_values(
            array_filter($events, function ($event) use ($reqYear, $reqMonth) {
                return isset($event->event_year) &&
                    isset($event->event_month) &&
                    $event->event_year == $reqYear &&
                    $event->event_month == $reqMonth;
            })
        );

        return json_encode($res);
    }

    /**
     * @return array The next event's info
     * @param array $events The complete events list, as found in the plugin configuration
     *
     * Returns all info about the next event
     */
    public function getNextEvent($events)
    {
        // rebuild avec :
        // si un event est complet on prend le suivant

        $currentDate = new DateTime();

        $minDate = null;
        $nextEvent = null;

        // Sort events by event_year, event_month and slots
        usort($events, function ($a, $b) {
            if ($a->event_year == $b->event_year) {
                if ($a->event_month == $b->event_month) {
                    sort($a->slots);
                    sort($b->slots);
                    return $a->slots[0] > $b->slots[0];
                } else {
                    return $a->event_month > $b->event_month;
                }
            } else {
                return $a->event_year > $b->event_year;
            }
        });

        // Find the next event past the current date
        foreach ($events as $event) {
            if (
                $event->availability == true &&
                ($event->event_type == "working_group" ||
                    $event->event_type == "formation")
            ) {
                $minSlotDate = null;
                $firstSlotDate = null;
                $maxSlotDate = null;
                foreach ($event->slots as $slot) {
                    $slotDate = new DateTime(
                        $event->event_year .
                            "-" .
                            $event->event_month .
                            "-" .
                            $slot
                    );
                    if (
                        $slotDate > $currentDate &&
                        (!isset($minSlotDate) || $slotDate < $minSlotDate)
                    ) {
                        $minSlotDate = $slotDate;
                    }
                    if (!isset($firstSlotDate) || $slotDate < $firstSlotDate) {
                        $firstSlotDate = $slotDate;
                    }
                    if (!isset($maxSlotDate) || $slotDate > $maxSlotDate) {
                        $maxSlotDate = $slotDate;
                    }
                }
                if (isset($minSlotDate)) {
                    if (!isset($minDate) || $minSlotDate < $minDate) {
                        $minDate = $minSlotDate;
                        $nextEvent = $event;
                        $nextEvent->firstDate = $firstSlotDate->format("Y-m-d");
                        $nextEvent->nextDate = $minSlotDate->format("Y-m-d");
                        $nextEvent->lastDate = $maxSlotDate->format("Y-m-d");
                    }
                }
            }
        }

        if (isset($nextEvent->firstDate)) {
            return $nextEvent;
        }
        return null;
    }

    /**
     * @return array The two next events' info
     * @param array $events The complete events list, as found in the plugin configuration
     *
     * Returns all info about the two next events
     */
    public function getTwoNextEvents($events)
    {
        $currentDate = date("Y-m-d");

        $currentYear = intval(date("Y"));
        $currentMonth = intval(date("m"));
        $currentDay = intval(date("d"));

        $minDate1 = null;
        $minDate2 = null;
        $nextEvent1 = [];
        $nextEvent2 = [];

        $res1 = [];
        $res2 = [];

        $minYear = null;

        if (isset($events["years"])) {
            foreach ($events["years"] as $year) {
                if ($year["year"] == $currentYear) {
                    for (
                        $monthNumber = $currentMonth;
                        $monthNumber <= 12;
                        $monthNumber++
                    ) {
                        if (isset($year["month_" . $monthNumber])) {
                            $month = $year["month_" . $monthNumber];
                            foreach ($month as $parsedEvent) {
                                if (
                                    isset($parsedEvent["slots"]) &&
                                    $parsedEvent["availability"] == true &&
                                    $parsedEvent["event_type"] ==
                                        "working_group"
                                ) {
                                    $minSlotDate = null;
                                    $firstSlotDate = null;
                                    $maxSlotDate = null;
                                    foreach ($parsedEvent["slots"] as $slot) {
                                        $slotDate =
                                            $year["year"] .
                                            "-" .
                                            $monthNumber .
                                            "-" .
                                            $slot["day"];
                                        if (
                                            date(
                                                "Y-m-d",
                                                strtotime($slotDate)
                                            ) > $currentDate &&
                                            (!isset($minSlotDate) ||
                                                date($slotDate) < $minSlotDate)
                                        ) {
                                            $minSlotDate = date(
                                                strtotime($slotDate)
                                            );
                                        }
                                        if (
                                            !isset($firstSlotDate) ||
                                            date(strtotime($slotDate)) <
                                                $firstSlotDate
                                        ) {
                                            $firstSlotDate = date(
                                                strtotime($slotDate)
                                            );
                                        }
                                        if (
                                            !isset($maxSlotDate) ||
                                            date(strtotime($slotDate)) >
                                                $maxSlotDate
                                        ) {
                                            $maxSlotDate = date(
                                                strtotime($slotDate)
                                            );
                                        }
                                    }
                                    if (isset($minSlotDate)) {
                                        if (!isset($minDate1)) {
                                            $minDate1 = date($minSlotDate);
                                            $nextEvent1 = $parsedEvent;
                                            $nextEvent1["firstDate"] = date(
                                                "Y-m-d",
                                                $firstSlotDate
                                            );
                                            $nextEvent1["nextDate"] = date(
                                                "Y-m-d",
                                                $minSlotDate
                                            );
                                            $nextEvent1["lastDate"] = date(
                                                "Y-m-d",
                                                $maxSlotDate
                                            );
                                        } elseif (
                                            $parsedEvent["id"] !=
                                                $nextEvent1["id"] &&
                                            (!isset($minDate2) ||
                                                date($minSlotDate) < $minDate2)
                                        ) {
                                            if ($minSlotDate < $minDate1) {
                                                if (
                                                    isset($nextEvent2["id"]) &&
                                                    $parsedEvent["id"] ==
                                                        $nextEvent2["id"]
                                                ) {
                                                    $nextEvent2[
                                                        "firstDate"
                                                    ] = date(
                                                        "Y-m-d",
                                                        $firstSlotDate
                                                    );
                                                    $nextEvent2[
                                                        "nextDate"
                                                    ] = date(
                                                        "Y-m-d",
                                                        $minSlotDate
                                                    );
                                                    $nextEvent2[
                                                        "lastDate"
                                                    ] = date(
                                                        "Y-m-d",
                                                        $maxSlotDate
                                                    );
                                                    $tempEvent = $nextEvent2;
                                                    $nextEvent2 = $nextEvent1;
                                                    $nextEvent1 = $tempEvent;
                                                } else {
                                                    $minDate2 = date($minDate1);
                                                    $nextEvent2 = $nextEvent1;
                                                    $minDate1 = date(
                                                        $minSlotDate
                                                    );
                                                    $nextEvent1 = $parsedEvent;
                                                    $nextEvent1[
                                                        "firstDate"
                                                    ] = date(
                                                        "Y-m-d",
                                                        $firstSlotDate
                                                    );
                                                    $nextEvent1[
                                                        "nextDate"
                                                    ] = date(
                                                        "Y-m-d",
                                                        $minSlotDate
                                                    );
                                                    $nextEvent1[
                                                        "lastDate"
                                                    ] = date(
                                                        "Y-m-d",
                                                        $maxSlotDate
                                                    );
                                                }
                                            } else {
                                                $minDate2 = date($minSlotDate);
                                                $nextEvent2 = $parsedEvent;
                                                $nextEvent2["firstDate"] = date(
                                                    "Y-m-d",
                                                    $firstSlotDate
                                                );
                                                $nextEvent2["nextDate"] = date(
                                                    "Y-m-d",
                                                    $minSlotDate
                                                );
                                                $nextEvent2["lastDate"] = date(
                                                    "Y-m-d",
                                                    $maxSlotDate
                                                );
                                            }
                                        }
                                    }
                                }
                            }
                            if (
                                isset($nextEvent1["firstDate"]) &&
                                isset($nextEvent2["firstDate"])
                            ) {
                                if (isset($nextEvent1["id"])) {
                                    $res1 = $this->getEventInfo(
                                        $events,
                                        $nextEvent1["id"]
                                    ); //parse all events to get first and last slots dates
                                    $res1["nextDate"] = $nextEvent1["nextDate"];
                                }
                                if (isset($nextEvent2["id"])) {
                                    $res2 = $this->getEventInfo(
                                        $events,
                                        $nextEvent2["id"]
                                    ); //parse all events to get first and last slots dates
                                    $res2["nextDate"] = $nextEvent2["nextDate"];
                                }
                                return [$res1, $res2];
                            }
                        }
                    }
                } elseif (
                    $year["year"] > $currentYear &&
                    (!isset($minYear) || $year["year"] < $minYear)
                ) {
                    $nextEvent["year_" . $year["year"]] = $year["year"];
                    for ($monthNumber = 1; $monthNumber <= 12; $monthNumber++) {
                        if (isset($year["month_" . $monthNumber])) {
                            $month = $year["month_" . $monthNumber];
                            foreach ($month as $parsedEvent) {
                                if (
                                    isset($parsedEvent["slots"]) &&
                                    $parsedEvent["availability"] == true &&
                                    $parsedEvent["event_type"] ==
                                        "working_group"
                                ) {
                                    $minSlotDate = null;
                                    $firstSlotDate = null;
                                    $maxSlotDate = null;
                                    foreach ($parsedEvent["slots"] as $slot) {
                                        $slotDate =
                                            $year["year"] .
                                            "-" .
                                            $monthNumber .
                                            "-" .
                                            $slot["day"];
                                        if (
                                            !isset($minSlotDate) ||
                                            date($slotDate) < $minSlotDate
                                        ) {
                                            $minSlotDate = date(
                                                strtotime($slotDate)
                                            );
                                        }
                                        if (
                                            !isset($firstSlotDate) ||
                                            date(strtotime($slotDate)) <
                                                $firstSlotDate
                                        ) {
                                            $firstSlotDate = date(
                                                strtotime($slotDate)
                                            );
                                        }
                                        if (
                                            !isset($maxSlotDate) ||
                                            date(strtotime($slotDate)) >
                                                $maxSlotDate
                                        ) {
                                            $maxSlotDate = date(
                                                strtotime($slotDate)
                                            );
                                        }
                                    }
                                    if (isset($minSlotDate)) {
                                        if (!isset($minDate1)) {
                                            $minDate1 = date($minSlotDate);
                                            $nextEvent1 = $parsedEvent;
                                            $nextEvent1["firstDate"] = date(
                                                "Y-m-d",
                                                $firstSlotDate
                                            );
                                            $nextEvent1["nextDate"] = date(
                                                "Y-m-d",
                                                $minSlotDate
                                            );
                                            $nextEvent1["lastDate"] = date(
                                                "Y-m-d",
                                                $maxSlotDate
                                            );
                                        } elseif (
                                            $parsedEvent["id"] !=
                                                $nextEvent1["id"] &&
                                            (!isset($minDate2) ||
                                                date($minSlotDate) < $minDate2)
                                        ) {
                                            if ($minSlotDate < $minDate1) {
                                                if (
                                                    isset($nextEvent2["id"]) &&
                                                    $parsedEvent["id"] ==
                                                        $nextEvent2["id"]
                                                ) {
                                                    $nextEvent2[
                                                        "firstDate"
                                                    ] = date(
                                                        "Y-m-d",
                                                        $firstSlotDate
                                                    );
                                                    $nextEvent2[
                                                        "nextDate"
                                                    ] = date(
                                                        "Y-m-d",
                                                        $minSlotDate
                                                    );
                                                    $nextEvent2[
                                                        "lastDate"
                                                    ] = date(
                                                        "Y-m-d",
                                                        $maxSlotDate
                                                    );
                                                    $tempEvent = $nextEvent2;
                                                    $nextEvent2 = $nextEvent1;
                                                    $nextEvent1 = $tempEvent;
                                                } else {
                                                    $minDate2 = date($minDate1);
                                                    $nextEvent2 = $nextEvent1;
                                                    $minDate1 = date(
                                                        $minSlotDate
                                                    );
                                                    $nextEvent1 = $parsedEvent;
                                                    $nextEvent1[
                                                        "firstDate"
                                                    ] = date(
                                                        "Y-m-d",
                                                        $firstSlotDate
                                                    );
                                                    $nextEvent1[
                                                        "nextDate"
                                                    ] = date(
                                                        "Y-m-d",
                                                        $minSlotDate
                                                    );
                                                    $nextEvent1[
                                                        "lastDate"
                                                    ] = date(
                                                        "Y-m-d",
                                                        $maxSlotDate
                                                    );
                                                }
                                            } else {
                                                $minDate2 = date($minSlotDate);
                                                $nextEvent2 = $parsedEvent;
                                                $nextEvent2["firstDate"] = date(
                                                    "Y-m-d",
                                                    $firstSlotDate
                                                );
                                                $nextEvent2["nextDate"] = date(
                                                    "Y-m-d",
                                                    $minSlotDate
                                                );
                                                $nextEvent2["lastDate"] = date(
                                                    "Y-m-d",
                                                    $maxSlotDate
                                                );
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (
                        isset($nextEvent1["firstDate"]) &&
                        isset($nextEvent2["firstDate"]) &&
                        $year["year"] == $currentYear + 1
                    ) {
                        if (isset($nextEvent1["id"])) {
                            $res1 = $this->getEventInfo(
                                $events,
                                $nextEvent1["id"]
                            ); //parse all events to get first and last slots dates
                            $res1["nextDate"] = $nextEvent1["nextDate"];
                        }
                        if (isset($nextEvent2["id"])) {
                            $res2 = $this->getEventInfo(
                                $events,
                                $nextEvent2["id"]
                            ); //parse all events to get first and last slots dates
                            $res2["nextDate"] = $nextEvent2["nextDate"];
                        }
                        return [$res1, $res2];
                    }
                }
            }
        }

        if (isset($nextEvent1["id"])) {
            $res1 = $this->getEventInfo($events, $nextEvent1["id"]); //parse all events to get first and last slots dates
            $res1["nextDate"] = $nextEvent1["nextDate"];
        }
        if (isset($nextEvent2["id"])) {
            $res2 = $this->getEventInfo($events, $nextEvent2["id"]); //parse all events to get first and last slots dates
            $res2["nextDate"] = $nextEvent2["nextDate"];
        }
        return [$res1, $res2];
    }
}

?>
