<?php
namespace Grav\Plugin;

use Composer\Autoload\ClassLoader;
use Grav\Common\Grav;
use Grav\Common\Plugin;
use Grav\Common\Twig\Twig;
use Grav\Common\Uri;
use RocketTheme\Toolbox\Event\Event;
use Grav\Plugin\ETDEventsCalendar\Calendar;
use Grav\Plugin\ETDEventsCalendar\Events;

/**
 * Class ETDEventsCalendarPlugin
 * @package Grav\Plugin
 */
class ETDEventsCalendarPlugin extends Plugin
{

    private $calendar;
    private $events;
    private $ajaxUrl="";

    /**
    * Base assets path.
    * @type string
    */
private $assetsPath = 'plugin://etd-events-calendar/assets/';

    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'onPluginsInitialized' => [
                ['autoload', 100000],
                ['onPluginsInitialized', 0],
            ],
            'onGetPageBlueprints' => ['onGetPageBlueprints', 0]
        ];
    }

    /**
     * Composer autoload
     *
     * @return ClassLoader
     */
    public function autoload(): ClassLoader
    {
        return require __DIR__ . '/vendor/autoload.php';
    }

    /**
     * Initialize the plugin
     */
    public function onPluginsInitialized(): void
    {
        // Don't proceed if we are in the admin plugin
        if ($this->isAdmin()) {
            return;
        }

        // Enable the main events we are interested in
        $this->enable([
            'onAdminTwigTemplatePaths' => ['onAdminTwigTemplatePaths', 0],
            'onPagesInitialized' => ['onPagesInitialized', 0],
            'onTwigInitialized' => ['onTwigInitialized', 0]
        ]);

        $this->calendar = new Calendar();
        $this->events = new Events();
    }

    /**
     * @param Event $event
     */
    public function onTwigInitialized(Event $event): void
    {
        if ($this->config->toArray()["plugins"]["etd-events-calendar"]["enabled"]==true) {
            /** @var Twig $gravTwig */
            $gravTwig = $this->grav['twig'];

            $gravTwig->twig()->addFunction(
                new \Twig_SimpleFunction('evt_getCalendar',[$this,'calendar'])
            );

            $gravTwig->twig()->addFunction(
                new \Twig_SimpleFunction('evt_getEventInfo', [$this, 'getEventInfo'])
            );

            $gravTwig->twig()->addFunction(
                new \Twig_SimpleFunction('evt_getNextEvent', [$this, 'getNextEvent'])
            );

            $gravTwig->twig()->addFunction(
                new \Twig_SimpleFunction('evt_getTwoNextEvents', [$this, 'getTwoNextEvents'])
            );
        }
    }

    public function onGetPageBlueprints($event)
    {
        $types = $event->types;
        $types->scanBlueprints('plugins://' . $this->name . '/blueprints');
    }

    /**
     * @return string Twig for displaying an empty calendar
     *
     * Displays an empty calendar on the page.
     * Called by the evt_getCalendar() function
     * In order to be displayed correctly, use : "evaluate_twig(evt_getCalendar()|raw)|raw"
     */
    public function calendar(): string
    {
        $twig = $this->grav['twig'];
        $assets = $this->grav['assets'];
        $pluginConfig = $this->config->toArray();

        // Add plugin files to the grav assets.
        $assets->addCss( $this->assetsPath . 'etd-events-calendar.css', 10, false);
        $assets->addJs( $this->assetsPath . 'calendar.js');

        $formLink="";
        if(isset($pluginConfig["plugins"]["etd-events-calendar"]["form_link"])){
            $formLink = $pluginConfig["plugins"]["etd-events-calendar"]["form_link"];
        }

        $translations=[];
        $translations["CURRENT_MONTH"] = $this->grav['language']->translate(['PLUGIN_ETD_EVENTS_CALENDAR.CURRENT_MONTH'], ['fr']);
        $translations["EVENT_FULL"] = $this->grav['language']->translate(['PLUGIN_ETD_EVENTS_CALENDAR.EVENT_FULL'], ['fr']);
        $translations["LEGEND_AVAILABLE"] = $this->grav['language']->translate(['PLUGIN_ETD_EVENTS_CALENDAR.LEGEND_AVAILABLE'], ['fr']);
        $translations["LEGEND_WORKING_GROUP"] = $this->grav['language']->translate(['PLUGIN_ETD_EVENTS_CALENDAR.LEGEND_WORKING_GROUP'], ['fr']);
        $translations["LEGEND_PRIVATISED"] = $this->grav['language']->translate(['PLUGIN_ETD_EVENTS_CALENDAR.LEGEND_PRIVATISED'], ['fr']);
        $translations["LEGEND_FORMATION"] = $this->grav['language']->translate(['PLUGIN_ETD_EVENTS_CALENDAR.LEGEND_FORMATION'], ['fr']);

        return $this->calendar->displayCalendar($translations);
    }

    /**
     * @return array The requested event's info
     * @param string $eventId The wanted event's id
     * Fetch an event by its identifier and returns its infos.
     */
    public function getEventInfo(string $eventId)
    {
        $events = $this->getEvents();
        $res = $this->events->getEventInfo($events, $eventId);
        return $res;
    }

    /**
     * @return array The next event's info
     * Fetch the future and available event with the closest date from the current date
     */
    public function getNextEvent()
    {
        $events = $this->getEvents();
        $res = $this->events->getNextEvent($events);
        return $res;
    }

     /**
     * @return array The two next events' info
     * Fetch the two future and available event with the closest date from the current date
     */
    public function getTwoNextEvents()
    {
        $events = $this->getEvents();
        $res = $this->events->getTwoNextEvents($events);
        return $res;
    }

    /**
     * @return string The result of the request
     * 
     * This function differentiates Ajax requests that are specific to this plugin, and standard requests.
     * Ajax requests must be made by reaching the path set in the
     */
    public function onPagesInitialized() : string
    {
        $uri = $this->grav['uri'];
        $pluginConfig = $this->config->toArray();
        $this->ajaxUrl = $pluginConfig["plugins"]["etd-events-calendar"]["ajax_link"];
        $res = 'err';
        if ($this->ajaxUrl && $this->ajaxUrl === $uri->path()) {
            $action='';
            $param='';
            if(isset($_GET['action'])){
                $action=$_GET['action'];
                if(isset($_GET['param'])){
                    $param=$_GET['param'];
                }
                $res= $this->handleRequest($action, $param);
            }
            echo $res;
            exit();
        }
        return $res;
    }

    /**
     * @return string The result of the Ajax request
     * @param string $action The request
     * @param string $param An optionnal parameter to the request
     * 
     * This function handles async requests
     */
    public function handleRequest(string $action, string $param): string{
        $res = '';
        switch($action){
            case 'getMonthlyEvents':
                $pluginConfig = $this->config->toArray();
                $events = $this->getEvents();
                $formLink="";
                if(isset($pluginConfig["plugins"]["etd-events-calendar"]["form_link"])){
                    $formLink = $pluginConfig["plugins"]["etd-events-calendar"]["form_link"];
                }
                $res = $this->events->getMonthlyEvents($param, $events, $formLink);
                break;
            default:
                $res = 'error; action='.$action.' ; param='.$param;
        }
        return $res;
    }

    private function getEvents() {
        $events = [];
        $page = Grav::instance()['page'];
        $collection = $page->evaluate([
            '@page.children' => $this->config->get("plugins.etd-events-calendar.events_folder"), 
            'type' => 'evenement', 
            'published' => true,
        ]);
        foreach ($collection as $page) {
            $event = $page->header();
            $event->url = $page->url();
            $event->id = $page->id();
            $events[] = $event;
        }
        return $events;
    }
}
