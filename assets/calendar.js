const baseUrl = window.location.protocol + "//" + window.location.hostname;

var currentDate = new Date();
var currentMonth =
  currentDate.getFullYear() + "-" + (currentDate.getMonth() + 1);

var viewedDate = new Date();
var viewedMonth = viewedDate.getFullYear() + "-" + (viewedDate.getMonth() + 1);

var initialized = false;

var eventFullText = "Complet";

var buttonTextIsSet = false;

fetchEvents();

function fetchEvents() {
  var xhttp = new XMLHttpRequest();
  var calendarBody = document.getElementById("etd-calendar-body");
  if (calendarBody) {
    calendarBody.style.opacity = "0";
  }

  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      var res = JSON.parse(this.responseText);

      setHeaderDate(viewedDate);
      fillCalendar(res);

      var calendarBody = document.getElementById("etd-calendar-body");
      if (calendarBody) {
        calendarBody.style.opacity = "1";
      }

      if (!initialized) {
        const prevMonthButton = document.getElementById("etd-previous-month");
        if (prevMonthButton) {
          prevMonthButton.addEventListener("click", function () {
            viewedDate.setMonth(viewedDate.getMonth() - 1);
            viewedMonth =
              viewedDate.getFullYear() + "-" + (viewedDate.getMonth() + 1);
            fetchEvents();
          });
        }

        const nextMonthButton = document.getElementById("etd-next-month");
        if (nextMonthButton) {
          nextMonthButton.addEventListener("click", function () {
            viewedDate.setMonth(viewedDate.getMonth() + 1);
            viewedMonth =
              viewedDate.getFullYear() + "-" + (viewedDate.getMonth() + 1);
            fetchEvents();
          });
        }

        const currentMonthButton = document.getElementById("etd-current-month");
        if (currentMonthButton) {
          currentMonthButton.addEventListener("click", function () {
            viewedDate = new Date(currentDate);
            viewedMonth =
              viewedDate.getFullYear() + "-" + (viewedDate.getMonth() + 1);
            fetchEvents();
          });
        }

        initialized = true;
      }
    }
  };

  xhttp.open(
    "GET",
    baseUrl + "/calendar-ajax?action=getMonthlyEvents&param=" + viewedMonth,
    true,
  );
  xhttp.send();
}

function getLang() {
  if (navigator.languages != undefined) return navigator.languages[0];
  return navigator.language;
}

function setHeaderDate(date) {
  const headerMonth = document.getElementById("etd-calendar-header-month");
  var headerDate = new Intl.DateTimeFormat(getLang(), {
    month: "long",
    year: "numeric",
  }).format(date);
  headerDate = headerDate.charAt(0).toUpperCase() + headerDate.slice(1);
  headerMonth.innerHTML = headerDate;
}

function fillCalendar(res) {
  const weeks = document.getElementsByClassName("week");

  var monthInfo = res["monthInfo"];
  var events = res["events"];

  var parsedDate = new Date(monthInfo["monthDate"]);

  var dayNumber = 0;

  //Suppression des dates et évènements dans les cases précédant le 1er jour du mois (si le mois ne commence pas un lundi)
  for (
    var weekDayNumber = 0;
    weekDayNumber < monthInfo["firstDay"];
    weekDayNumber++
  ) {
    var day = weeks[0].getElementsByClassName("day-n-" + weekDayNumber)[0];
    if (day) {
      day.removeAttribute("href");
      day.classList.add("empty");
      var event = day.getElementsByClassName("event")[0];
      if (event) {
        event.remove();
      }
      var dateSpan = day.getElementsByClassName("date")[0];
      if (dateSpan) {
        dateSpan.innerHTML = "";
      }
      var emptySlot = day.getElementsByClassName("empty-slot")[0];
      if (emptySlot) {
        emptySlot.remove();
      }
      var tick = day.getElementsByClassName("tick")[0];
      if (tick) {
        tick.remove();
      }
    }
    dayNumber++;
  }

  var previousEventId = "";
  var consecutiveSlots = 1;
  var followsWeekBeginning = false;

  //elmt.classList.add("2-consecutive");

  //Remplissage des jours de la première semaine
  for (
    var weekDayNumber = monthInfo["firstDay"];
    weekDayNumber < 7;
    weekDayNumber++
  ) {
    var day = weeks[0].getElementsByClassName("day-n-" + weekDayNumber)[0];
    if (day) {
      day.classList.remove("empty");
      day.removeAttribute("href");

      var emptySlot = day.getElementsByClassName("empty-slot")[0];
      if (emptySlot) {
        emptySlot.remove();
      }
      var tick = day.getElementsByClassName("tick")[0];
      if (tick) {
        tick.remove();
      }

      var dateSpan = day.getElementsByClassName("date")[0];
      if (dateSpan) {
        var displayDate = new Intl.DateTimeFormat(getLang(), {
          weekday: "long",
          day: "numeric",
        }).format(parsedDate);
        dateSpan.innerHTML =
          displayDate.charAt(0).toUpperCase() + displayDate.slice(1);
      }

      //Suppression de l'évènement précédent
      var eventToDelete = day.getElementsByClassName("event")[0];
      if (eventToDelete) {
        eventToDelete.remove();
      }

      //Affichage de l'évènement du jour
      var hasEvent = false;
      var tempEvent;
      var shortParsedDate =
        parsedDate.getFullYear() +
        "-" +
        new Intl.DateTimeFormat(getLang(), { month: "2-digit" }).format(
          parsedDate,
        ) +
        "-" +
        new Intl.DateTimeFormat(getLang(), { day: "2-digit" }).format(
          parsedDate,
        );
      if (Array.isArray(events)) {
        events.forEach((event) => {
          if (event["slots"]) {
            event["slots"].forEach((slot) => {
              if (
                slot ===
                new Intl.DateTimeFormat(getLang(), { day: "numeric" }).format(
                  parsedDate,
                )
              ) {
                tempEvent = event;
                hasEvent = true;

                var eventId = event["id"].split(" ").join("_");
                //On ajoute l'évènement
                if (eventId == previousEventId) {
                  consecutiveSlots++;
                } else {
                  consecutiveSlots = 1;
                }

                /*
                //ANCIEN CODE (pour réservation ateliers / formations depuis formulaire)
                if(parsedDate < currentDate){
                  day.innerHTML += "<div class='event event-"+event["id"]+" unavailable " + connectEvents(weekDayNumber, dayNumber, event["id"]) + " consecutive-slot-" + consecutiveSlots + "'><span>"+event['title']+"</span></div>";
                }else if (event["internal_page"]){
                  day.innerHTML += "<a href='"+event["internal_page"]+"?eventId="+event["id"]+"' class='event event-"+event["id"] + connectEvents(weekDayNumber, dayNumber, event["id"]) + " consecutive-slot-" + consecutiveSlots + "'><span>"+event['title']+"</span></a>";
                }else if (slot["availability"]){
                  day.innerHTML += "<a href='"+monthInfo["formLink"]+"?eventId="+event["id"]+"&eventType="+event["event_type"]+"&date="+shortParsedDate+"' class='event event-"+event["id"] + connectEvents(weekDayNumber, dayNumber, event["id"]) + " consecutive-slot-" + consecutiveSlots + "'><span>"+event['title']+"</span></a>";
                }else{
                  day.innerHTML += "<div class='event event-"+event["id"]+" unavailable" + connectEvents(weekDayNumber, dayNumber, event["id"]) + " consecutive-slot-" + consecutiveSlots + "'><span>"+event['title']+"</span></div>";
                }
                */

                var eventElmt;
                var eventTitle = document.createElement("span");
                var eventTitleText = document.createTextNode(event["title"]);

                if (event["event_type"] == "privatisation") {
                  eventElmt = document.createElement("div");
                  eventElmt.classList.add("privatised");
                  eventTitle.appendChild(eventTitleText);
                  eventElmt.appendChild(eventTitle);
                } else {
                  eventElmt = document.createElement("a");
                  eventTitle.appendChild(eventTitleText);
                  eventElmt.appendChild(eventTitle);

                  if (event["event_type"] == "formation") {
                    eventElmt.classList.add("formation");
                    if (event["availability"] == 0) {
                      var fullTag = document.createElement("span");
                      fullTag.appendChild(
                        document.createTextNode(eventFullText),
                      );
                      fullTag.classList.add("full-tag");
                      eventElmt.appendChild(fullTag);
                    }
                  } else if (event["event_type"] == "working_group") {
                    eventElmt.classList.add("working-group");
                    if (event["availability"] == 0) {
                      var fullTag = document.createElement("span");
                      fullTag.appendChild(
                        document.createTextNode(eventFullText),
                      );
                      fullTag.classList.add("full-tag");
                      eventElmt.appendChild(fullTag);
                    }
                  } else if (event["event_type"] == "exterieur") {
                    eventElmt.classList.add("exterieur");
                    if (event["availability"] == 0) {
                      var fullTag = document.createElement("span");
                      fullTag.appendChild(
                        document.createTextNode(eventFullText),
                      );
                      fullTag.classList.add("full-tag");
                      eventElmt.appendChild(fullTag);
                    }
                  }
                  if (event["link"] == "internal") {
                    eventElmt.href = event["url"];
                  } else if (
                    event["link"] == "external" &&
                    event["external_page"].trim().length > 0
                  ) {
                    eventElmt.href = event["external_page"];
                    eventElmt.target = "_blank";
                  }
                }

                eventElmt.classList.add("event");
                eventElmt.classList.add("event-" + eventId);
                var connectEventsRes = connectEvents(
                  weekDayNumber,
                  dayNumber,
                  eventId,
                );
                if (connectEventsRes) {
                  eventElmt.classList.add(connectEventsRes);
                  if (connectEventsRes == "following-week-begining") {
                    eventElmt.classList.add("follow-prev-week");
                  }
                }
                eventElmt.classList.add("consecutive-slot-" + consecutiveSlots);

                day.appendChild(eventElmt);

                //------------------

                previousEventId = eventId;
                return;
              }
            });
          }
        });
      }
      if (!hasEvent) {
        consecutiveSlots = 1;
        previousEventId = "";
        var innerHTML = "<div class='empty-slot'></div>";
        //day.setAttribute("href", monthInfo["formLink"]+"?eventType=privatisation&date="+shortParsedDate); //1
        //var innerHTML = "<a class='empty-slot";     //1
        //var innerHTML = "<a class='empty-slot'";    //2
        //if(parsedDate > currentDate){               //2
        //innerHTML+="' href='"+monthInfo["formLink"]+"?eventType=privatisation&date="+shortParsedDate+"'";   //1
        //innerHTML+=" href='"+monthInfo["formLink"]+"?eventType=privatisation&date="+shortParsedDate+"'";    //2
        //}                         //2
        //else{                     //1
        //  innerHTML+=" past'";    //1
        //}                         //1
        //innerHTML += "></a>";     //2

        if (parsedDate > currentDate) {
          day.href =
            monthInfo["formLink"] +
            "?eventType=privatisation&date=" +
            shortParsedDate;
        }
        day.innerHTML += innerHTML;
      }

      if (parsedDate <= currentDate) {
        var tick;
        if (hasEvent) {
          tick = document.createElement("a");
          if (tempEvent["link"] == "internal") {
            tick.href = tempEvent["url"];
          } else if (tempEvent["link"] == "external") {
            tick.href = tempEvent["external_page"];
            tick.target = "_blank";
          }
        } else {
          tick = document.createElement("div");
        }
        tick.classList.add("tick");
        day.appendChild(tick);
      }
    }
    parsedDate.setDate(parsedDate.getDate() + 1);
    dayNumber++;
  }

  //Remplissage des jours suivants
  var monthOver = false;
  for (var weekNumber = 1; weekNumber < weeks.length; weekNumber++) {
    previousEventId = "";
    consecutiveSlots = 1;
    let currentWeek = document.getElementById("week-n-" + weekNumber);
    if (currentWeek) {
      currentWeek.style.display = "flex";
    }
    for (var weekDayNumber = 0; weekDayNumber < 7; weekDayNumber++) {
      if (!monthOver) {
        //Affichage de la date dans la case
        var day = weeks[weekNumber].getElementsByClassName(
          "day-n-" + weekDayNumber,
        )[0];
        if (day) {
          var emptySlot = day.getElementsByClassName("empty-slot")[0];
          if (emptySlot) {
            emptySlot.remove();
          }
          var tick = day.getElementsByClassName("tick")[0];
          if (tick) {
            tick.remove();
          }
          day.removeAttribute("href");
          var dateSpan = day.getElementsByClassName("date")[0];
          if (dateSpan) {
            var displayDate = new Intl.DateTimeFormat(getLang(), {
              weekday: "long",
              day: "numeric",
            }).format(parsedDate);
            dateSpan.innerHTML =
              displayDate.charAt(0).toUpperCase() + displayDate.slice(1);
          }

          //Suppression de l'évènement précédent
          var eventToDelete = day.getElementsByClassName("event")[0];
          if (eventToDelete) {
            eventToDelete.remove();
          }

          //Affichage de l'évènement du jour
          var hasEvent = false;
          var shortParsedDate =
            parsedDate.getFullYear() +
            "-" +
            new Intl.DateTimeFormat(getLang(), { month: "2-digit" }).format(
              parsedDate,
            ) +
            "-" +
            new Intl.DateTimeFormat(getLang(), { day: "2-digit" }).format(
              parsedDate,
            );
          var tempEvent;
          day.classList.remove("empty");
          if (Array.isArray(events)) {
            events.forEach((event) => {
              if (event["slots"]) {
                event["slots"].forEach((slot) => {
                  if (
                    slot ==
                    new Intl.DateTimeFormat(getLang(), {
                      day: "numeric",
                    }).format(parsedDate)
                  ) {
                    hasEvent = true;
                    tempEvent = event;

                    var eventId = event["id"].split(" ").join("_");
                    //On ajoute l'évènement
                    if (eventId == previousEventId) {
                      consecutiveSlots++;
                    } else {
                      consecutiveSlots = 1;
                    }

                    /*
                    //ANCIEN CODE (pour réservation ateliers / formations depuis formulaire)
                    if(parsedDate < currentDate){
                      day.innerHTML += "<div class='event event-"+event["id"]+" unavailable " + connectEvents(weekDayNumber, dayNumber, event["id"]) + " consecutive-slot-" + consecutiveSlots + "'><span>"+event['title']+"</span></div>";
                    }else if (event["internal_page"]){
                      day.innerHTML += "<a href='"+event["internal_page"]+"?eventId="+event["id"]+"' class='event event-"+event["id"] + connectEvents(weekDayNumber, dayNumber, event["id"]) + " consecutive-slot-" + consecutiveSlots + "'><span>"+event['title']+"</span></a>";
                    }else if (slot["availability"]){
                      day.innerHTML += "<a href='"+monthInfo["formLink"]+"?eventId="+event["id"]+"&eventType="+event["event_type"]+"&date="+shortParsedDate+"' class='event event-"+event["id"] + connectEvents(weekDayNumber, dayNumber, event["id"]) + " consecutive-slot-" + consecutiveSlots + "'><span>"+event['title']+"</span></a>";
                    }else{
                      day.innerHTML += "<div class='event event-"+event["id"]+" unavailable" + connectEvents(weekDayNumber, dayNumber, event["id"]) + " consecutive-slot-" + consecutiveSlots + "'><span>"+event['title']+"</span></div>";
                    }
                    */

                    var eventElmt;
                    var eventTitle = document.createElement("span");
                    var eventTitleText = document.createTextNode(
                      event["title"],
                    );

                    if (event["event_type"] == "privatisation") {
                      eventElmt = document.createElement("div");
                      eventElmt.classList.add("privatised");
                      eventTitle.appendChild(eventTitleText);
                      eventElmt.appendChild(eventTitle);
                    } else {
                      eventElmt = document.createElement("a");
                      eventTitle.appendChild(eventTitleText);
                      eventElmt.appendChild(eventTitle);
                      if (event["event_type"] == "formation") {
                        eventElmt.classList.add("formation");
                        if (event["availability"] == 0) {
                          var fullTag = document.createElement("span");
                          fullTag.appendChild(
                            document.createTextNode(eventFullText),
                          );
                          fullTag.classList.add("full-tag");
                          eventElmt.appendChild(fullTag);
                        }
                      } else if (event["event_type"] == "working_group") {
                        eventElmt.classList.add("working-group");
                        if (event["availability"] == 0) {
                          var fullTag = document.createElement("span");
                          fullTag.appendChild(
                            document.createTextNode(eventFullText),
                          );
                          fullTag.classList.add("full-tag");
                          eventElmt.appendChild(fullTag);
                        }
                      } else if (event["event_type"] == "exterieur") {
                        eventElmt.classList.add("exterieur");
                        if (event["availability"] == 0) {
                          var fullTag = document.createElement("span");
                          fullTag.appendChild(
                            document.createTextNode(eventFullText),
                          );
                          fullTag.classList.add("full-tag");
                          eventElmt.appendChild(fullTag);
                        }
                      }
                      if (event["link"] == "internal") {
                        eventElmt.href = event["url"];
                      } else if (
                        event["link"] == "external" &&
                        event["external_page"].trim().length > 0
                      ) {
                        eventElmt.href = event["external_page"];
                        eventElmt.target = "_blank";
                      }
                    }

                    eventElmt.classList.add("event");
                    eventElmt.classList.add("event-" + eventId);
                    var connectEventsRes = connectEvents(
                      weekDayNumber,
                      dayNumber,
                      eventId,
                    );
                    if (connectEventsRes) {
                      eventElmt.classList.add(connectEventsRes);
                      if (connectEventsRes == "following-week-begining") {
                        eventElmt.classList.add("follow-prev-week");
                      }
                    }
                    eventElmt.classList.add(
                      "consecutive-slot-" + consecutiveSlots,
                    );

                    day.appendChild(eventElmt);

                    var previousDay = document.getElementById(
                      "date-" + (dayNumber - 1),
                    );
                    if (previousDay) {
                      var previousEvent = previousDay.getElementsByClassName(
                        "event-" + eventId,
                      )[0];
                      if (previousEvent) {
                        if (
                          previousEvent.classList.contains("follow-prev-week")
                        ) {
                          eventElmt.classList.add("follow-prev-week");
                        }
                      }
                    }

                    if (weekDayNumber == 0) {
                      precDay =
                        weeks[weekNumber - 1].getElementsByClassName(
                          "day-n-6",
                        )[0];
                    } else {
                      precDay = weeks[weekNumber].getElementsByClassName(
                        "day-n-" + (weekDayNumber - 1),
                      )[0];
                    }

                    //Si l'évènement se déroule également le jour précédent, on ajoute la classe en question
                    if (
                      weekNumber > 0 ||
                      weekDayNumber > monthInfo["firstDay"]
                    ) {
                      var precDay;
                      if (weekDayNumber == 0) {
                        precDay =
                          weeks[weekNumber - 1].getElementsByClassName(
                            "day-n-6",
                          )[0];
                      } else {
                        precDay = weeks[weekNumber].getElementsByClassName(
                          "day-n-" + (weekDayNumber - 1),
                        )[0];
                      }
                    }

                    //Si l'évènement se déroule également le jour suivant, on ajoute la classe en question
                    if (
                      weekNumber < 5 ||
                      weekDayNumber > monthInfo["lastDay"]
                    ) {
                      var precDay;
                      if (weekDayNumber == 6) {
                        precDay =
                          weeks[weekNumber + 1].getElementsByClassName(
                            "day-n-6",
                          )[0];
                      } else {
                        precDay = weeks[weekNumber].getElementsByClassName(
                          "day-n-" + (weekDayNumber + 1),
                        )[0];
                      }
                    }

                    previousEventId = eventId;
                    return;
                  }
                });
              }
            });
          }

          if (!hasEvent) {
            consecutiveSlots = 1;
            previousEventId = "";
            var innerHTML = "<div class='empty-slot'></div>";
            if (parsedDate > currentDate) {
              day.href =
                monthInfo["formLink"] +
                "?eventType=privatisation&date=" +
                shortParsedDate;
            }
            day.innerHTML += innerHTML;
          }

          if (parsedDate <= currentDate) {
            var tick;
            if (hasEvent) {
              tick = document.createElement("a");
              if (tempEvent["link"] == "internal") {
                tick.href = tempEvent["url"];
              } else if (tempEvent["link"] == "external") {
                tick.href = tempEvent["external_page"];
                tick.target = "_blank";
              }
            } else {
              tick = document.createElement("div");
            }
            tick.classList.add("tick");
            day.appendChild(tick);
          }
        }

        //Vérif mois fini
        var tempMonth = parsedDate.getMonth();
        parsedDate.setDate(parsedDate.getDate() + 1);
        if (parsedDate.getMonth() != tempMonth) {
          monthOver = true;
        }
      } else {
        if (weekDayNumber == 0) {
          let weekToHide = document.getElementById("week-n-" + weekNumber);
          if (weekToHide) {
            weekToHide.style.display = "none";
          }
        }

        //Suppression des dates et évènements pour les jours suivant le dernier du mois
        var day = weeks[weekNumber].getElementsByClassName(
          "day-n-" + weekDayNumber,
        )[0];
        if (day) {
          day.removeAttribute("href");
          day.classList.add("empty");

          var emptySlot = day.getElementsByClassName("empty-slot")[0];
          if (emptySlot) {
            emptySlot.remove();
          }
          var tick = day.getElementsByClassName("tick")[0];
          if (tick) {
            tick.remove();
          }

          var event = day.getElementsByClassName("event")[0];
          if (event) {
            event.remove();
          }
          var dateSpan = day.getElementsByClassName("date")[0];
          if (dateSpan) {
            dateSpan.innerHTML = "";
          }
        }
        parsedDate.setDate(parsedDate.getDate() + 1);
      }
      dayNumber++;
    }
  }
}

function connectEvents(weekDayNumber, dayNumber, eventId) {
  var res = "";
  if (dayNumber > 0) {
    var previousDay = document.getElementById("date-" + (dayNumber - 1));
    if (previousDay) {
      var previousEvent = previousDay.getElementsByClassName(
        "event-" + eventId,
      )[0];
      if (previousEvent) {
        if (weekDayNumber > 0) {
          res += "following";
          previousEvent.classList.add("preceding");
        } else {
          res += "following-week-begining";
          previousEvent.classList.add("preceding-week-end");
        }
      }
    }
  }

  return res;
}
